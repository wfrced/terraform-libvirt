# DHCP is required for some cloud image types,
# but we're using Ubuntu and don't need it
resource "libvirt_network" "terraform_network" {
  name      = "terraform_network"
  mode      = "nat"
  bridge    = "virbr_terraform"
  addresses = var.private_network_addresses
  dns { enabled = true }
  dhcp { enabled = false }
}
