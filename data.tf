# Cloud-init templates are generated for appropriate role
# mirror_ip is for repository mirror
data "template_file" "user_data" {
  for_each = var.roles

  template = file("cloud-init-templates/cloud_init-${each.value.name}.cfg")
  vars = {
    mirror_ip = var.mirror_ip
  }
}

# Cloud-init network configuration is the same across all
# hosts, but we want to define static IPs
data "template_file" "network_config" {
  for_each = var.servers

  template = file("cloud-init-templates/network_config.cfg")
  vars = {
    ip = each.value.ip
  }
}
