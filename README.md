# terraform-libvirt
*WIP*

A simple module to provision servers on Libvirt hypervisors for use with Rancher/Kubernetes.

Variables required:

* `roles`: a list of roles. Required parameters for a role are `name`, `memory`, `vcpu`, `disk_size`, and `image_source`.
* `servers`: a list of servers. Required parameters for a role are `name`, `role`, and `ip`.
* `private_network_addresses`: a list of ip addresses for private network that will belong to the servers. DHCP and DNS are on.
* `mirror_ip`: IP address of a Nexus OSS server (for proxying system and Docker packages).
