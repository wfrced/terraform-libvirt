resource "libvirt_domain" "server" {
  for_each = var.servers

  name   = each.value.name
  # Parameters defined by role
  memory = "${var.roles["${each.value.role}"].memory}"
  vcpu   = "${var.roles["${each.value.role}"].vcpu}"

  disk {
    volume_id = "${libvirt_volume.tf-os["${each.value.name}"].id}"
  }
  cloudinit = libvirt_cloudinit_disk.cloudinit["${each.value.name}"].id

  network_interface {
    network_id     = libvirt_network.terraform_network.id
    hostname       = each.value.name
    wait_for_lease = true
  }

  # Required for cloud images
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }
  # Required for Ubuntu cloud images
  qemu_agent = true
}
