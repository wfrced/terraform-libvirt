# Generate a volume from a basic cloud-init image
# and expand it
resource "libvirt_volume" "tf-os" {
  for_each = var.servers

  name             = "${each.value.name}-os.qcow2"
  base_volume_name = "${var.roles["${each.value.role}"].image_source}"
  size             = "${var.roles["${each.value.role}"].disk_size}"
  format           = "qcow2"
}

# Generate a volume with data for cloud-init
resource "libvirt_cloudinit_disk" "cloudinit" {
  for_each = var.servers

  name           = "cloudinit-${each.value.name}.iso"
  user_data      = data.template_file.user_data["${each.value.role}"].rendered
  network_config = data.template_file.network_config["${each.value.name}"].rendered
}
